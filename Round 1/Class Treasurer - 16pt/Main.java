import java.util.Scanner;

public class Main
{
    static void output(int testcase, String answer)
    {
        System.out.printf("Case #%d: %s\n", testcase + 1, answer);
    }
	public static void main(String[] args) throws Exception
	{
		Scanner scanner = new Scanner(System.in);
		int T = scanner.nextInt();
        long modulo = 1_000_000_007L;
        long[] costs = new long[1_000_001];
        costs[0] = 1L;
        for (int i = 1; i < costs.length; i++) {
            costs[i] = costs[i - 1] * 2L % modulo;
        }
		for (int tnum = 0; tnum < T; tnum++)
		{
			String answer = "";
            int n = scanner.nextInt();
            int k = scanner.nextInt();
            char[] v = scanner.next().toCharArray();
            long ans = 0L;
            int e = v.length - 1;
            int ac = 0, bc = 0;
            for (int i = v.length - 1; i >= 0; i--) {
                if (v[i] == 'A') {
                    if (e == i) {
                        e--;
                    } else {
                        ac++;
                        while (e >= i && ac >= bc) {
                            if (v[e] == 'A') {
                                ac--;
                            } else {
                                bc--;
                            }
                            e--;
                        }
                    }
                    continue;
                }
                bc++;
                if (bc > ac + k) {
                    v[i] = 'A';
                    bc--;
                    ac++;
                    ans += costs[i + 1];
                    ans %= modulo;
                }
                while (e >= i && ac >= bc) {
                    if (v[e] == 'A') {
                        ac--;
                    } else {
                        bc--;
                    }
                    e--;
                }
            }
            // System.err.println(new String(v));
            answer = Long.toString(ans);
			output(tnum, answer);
		}
	}
}