import java.util.Arrays;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Main
{
    static void output(int testcase, String answer)
    {
        System.out.printf("Case #%d: %s\n", testcase + 1, answer);
    }
	public static void main(String[] args) throws Exception
	{
		Scanner scanner = new Scanner(System.in);
		int T = scanner.nextInt();
		for (int tnum = 0; tnum < T; tnum++)
		{
			String answer = "Impossible";
            int n = scanner.nextInt();
            int m = scanner.nextInt();
            int[][] requirements = new int[n + 1][n + 1];
            int[][] connects = new int[n + 1][n + 1];
            int[][] dp = new int[n + 1][n + 1];
            for (int i = 0; i <= n; i++) {
                Arrays.fill(dp[i], 100_000_000);
                dp[i][i] = 0;
            }
            for (int i = 0; i < m; i++) {
                int x = scanner.nextInt();
                int y = scanner.nextInt();
                int z = scanner.nextInt();
                requirements[x][y] = z;
                requirements[y][x] = z;
                dp[x][y] = z;
                dp[y][x] = z;
                connects[x][y] = y;
                connects[y][x] = x;
            }
            for (int i = 1; i <= n; i++) {
                for (int j = 1; j <= n; j++) {
                    for (int k = 1; k <= n; k++) {
                        if (dp[j][i] + dp[i][k] < dp[j][k]) {
                            dp[j][k] = dp[j][i] + dp[i][k];
                            connects[j][k] = i;
                        }
                    }
                }
            }
            boolean ok = true;
            Map<Integer, Integer> edges = new TreeMap<>();
            for (int i = 1; i <= n; i++) {
                for (int j = 1; j <= n; j++) {
                    if (dp[i][j] < requirements[i][j]) {
                        ok = false;
                    }
                    if (ok && connects[i][j] == j) {
                        if (i < j) {
                            edges.put(i * 100 + j, dp[i][j]);
                        } else {
                            edges.put(j * 100 + i, dp[i][j]);
                        }
                    }
                }
            }
            if (ok) {
                StringBuilder sb = new StringBuilder();
                sb.append(edges.size());
                for (Map.Entry<Integer, Integer> e : edges.entrySet()) {
                    sb.append(String.format(
                        "\n%d %d %d",
                        e.getKey() / 100,
                        e.getKey() % 100,
                        e.getValue()
                    ));
                }
                answer = sb.toString();
            }
			output(tnum, answer);
		}
	}
}