import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class Main
{
    static void output(int testcase, String answer)
    {
        System.out.printf("Case #%d: %s\n", testcase + 1, answer);
    }
	public static void main(String[] args) throws Exception
	{
		Scanner scanner = new Scanner(System.in);
		int T = scanner.nextInt();
		for (int tnum = 0; tnum < T; tnum++)
		{
			String answer = "-1";
            int n = scanner.nextInt();
            int h = scanner.nextInt();
            int[] x = new int[n];
            int[] a = new int[n];
            int[] b = new int[n];
            Set<Integer> xs = new TreeSet<>();
            Set<Integer> ys = new TreeSet<>();
            xs.add(0);
            ys.add(0);
            ys.add(h);
            boolean ok = true;
            for (int i = 0; i < n; i++) {
                x[i] = scanner.nextInt();
                a[i] = scanner.nextInt();
                b[i] = scanner.nextInt();
                xs.add(x[i]);
                ys.add(a[i]);
                ys.add(b[i]);
                if (a[i] == 0 && b[i] == h) {
                    ok = false;
                }
            }
            int[] xo = xs.stream().mapToInt(Integer::intValue).toArray();
            int[] yo = ys.stream().mapToInt(Integer::intValue).toArray();
            int ww = xo.length;
            int hh = yo.length;
            int[] xx = new int[n];
            int[] aa = new int[n];
            int[] bb = new int[n];
            boolean[] lad = new boolean[hh];
            for (int i = 0; i < n; i++) {
                xx[i] = Arrays.binarySearch(xo, x[i]);
                aa[i] = Arrays.binarySearch(yo, a[i]);
                bb[i] = Arrays.binarySearch(yo, b[i]);
                for (int y = aa[i]; y < bb[i]; y++) {
                    lad[y] = true;
                }
                if (xx[i] < 0 || aa[i] < 0 || bb[i] < 0) {
                    // throw new RuntimeException("bad");
                    ok = false;
                    break;
                }
            }
            for (int y = 0; y < hh - 1; y++) {
                if (!lad[y]) {
                    ok = false;
                    answer = "0";
                    break;
                }
            }
            if (ok) {
                int ans = Integer.MAX_VALUE;
                for (int yi = yo.length - 1; yi > 0; yi--) {
                    List<Integer> ls = new ArrayList<>();
                    int[][] map = new int[hh][ww];
                    int minY = yo.length;
                    for (int i = 0; i < n; i++) {
                        if (bb[i] >= yi) {
                            for (int ey = aa[i]; ey <= bb[i]; ey++) {
                                map[ey][xx[i]] = 1;
                            }
                            ls.add(xx[i] * n + i);
                            minY = Math.min(minY, aa[i]);
                        }
                    }
                    if (minY == 0) {
                        break;
                    }
                    if (ls.isEmpty()) {
                        continue;
                    }
                    int cnt = 0;
                    for (int i = 0; i < n; i++) {
                        if (minY <= bb[i] && bb[i] < yi) {
                            for (int ey = aa[i]; ey <= bb[i]; ey++) {
                                map[ey][xx[i]] |= 2;
                            }
                            if (aa[i] < minY) {
                                cnt++;
                            }
                        }
                    }
                    if (cnt == 0) {
                        ans = 0;
                        break;
                    }
                    Collections.sort(ls);
                    int cost = 0;
                    for (int li : ls) {
                        int i = li % n;
                        boolean foundLad = true;
                        for (int ey = minY; ey < aa[i]; ey++) {
                            boolean found = false;
                            for (int xi = xx[i]; xi >= 0; xi--) {
                                if ((map[ey][xi] & 1) != 0) {
                                    break;
                                }
                                if ((map[ey + 1][xi] & 2) != 0) {
                                    found = true;
                                    break;
                                }
                            }
                            for (int xi = xx[i]; xi < xo.length; xi++) {
                                if ((map[ey][xi] & 1) != 0) {
                                    break;
                                }
                                if ((map[ey + 1][xi] & 2) != 0) {
                                    found = true;
                                    break;
                                }
                            }
                            if (!found) {
                                foundLad = false;
                                break;
                            }
                        }
                        if (!foundLad) {
                            continue;
                        }
                        for (int ey = aa[i]; ey < bb[i]; ey++) {
                            boolean found = false;
                            for (int xi = xx[i] - 1; xi >= 0; xi--) {
                                if ((map[ey][xi] & 1) != 0) {
                                    break;
                                }
                                if ((map[ey + 1][xi] & 2) != 0) {
                                    found = true;
                                    cost += yo[ey + 1] - yo[ey];
                                    break;
                                }
                            }
                            if (!found) {
                                break;
                            }
                        }
                        for (int ey = aa[i]; ey < bb[i]; ey++) {
                            boolean found = false;
                            for (int xi = xx[i] + 1; xi < xo.length; xi++) {
                                if ((map[ey][xi] & 1) != 0) {
                                    break;
                                }
                                if ((map[ey + 1][xi] & 2) != 0) {
                                    found = true;
                                    cost += yo[ey + 1] - yo[ey];
                                    break;
                                }
                            }
                            if (!found) {
                                break;
                            }
                        }
                    }
                    ans = Math.min(ans, cost);
                }
                if (ans == Integer.MAX_VALUE) {
                    ans = 0;
                }
                answer = Integer.toString(ans);
            }
			output(tnum, answer);
		}
	}
}