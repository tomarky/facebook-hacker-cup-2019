Facebook Hacker Cup 2019に参加した時のコード

Qualification Round 2019/06/15 ～ 2019/06/17 - 55pt 146位 (9:18:54)
    Leapfrog: Ch. 1 - 15pt OK
    Leapfrog: Ch. 2 - 15pt OK
    Mr. X - 25pt OK
    Trees as a Service - 45pt read and gave up
https://www.facebook.com/hackercup/scoreboard/1015632918646326/?filter=everyone&offset=100



Round 1 2019/06/30 ～ 2019/07/01 - 31pt 932位 (23:39:08)
    Graphs as a Service - 15pt OK
	Class Treasurer - 16pt OK
	Ladders and Snakes - 34pt NG
	Connect the Dots - 35pt no read
https://www.facebook.com/hackercup/scoreboard/312469622734026/?filter=everyone&offset=900


Round 2 2019/07/14 02:00 ～ 05:00 (UTC+9) - 14pt 848位 (34:59)
    On the Run - 14pt OK
    Bitstrings as a Service - 20pt NG
    Grading - 29pt read and gave up
    Seafood - 37pt no read
https://www.facebook.com/hackercup/scoreboard/414796186005449/?filter=everyone&offset=800


