import java.util.Scanner;

public class Main
{
    static void output(int testcase, String answer)
    {
        System.out.printf("Case #%d: %s\n", testcase + 1, answer);
    }
	public static void main(String[] args) throws Exception
	{
		Scanner scanner = new Scanner(System.in);
		int T = scanner.nextInt();
		for (int tnum = 0; tnum < T; tnum++)
		{
			String answer = "N";
            int n = scanner.nextInt();
            int m = scanner.nextInt();
            int k = scanner.nextInt();
            int a = scanner.nextInt();
            int b = scanner.nextInt();
            int[] r = new int[k];
            int[] c = new int[k];
            for (int i = 0; i < k; i++) {
                r[i] = scanner.nextInt();
                c[i] = scanner.nextInt();
            }
            if (k > 1) {
                int[] cnt = new int[2];
                for (int i = 0; i < k; i++) {
                    for (int j = i + 1; j < k; j++) {
                        int dist = Math.abs(r[i] - r[j]);
                        dist += Math.abs(c[i] - c[j]);
                        cnt[dist % 2]++;
                    }
                    int dist = Math.abs(r[i] - a);
                    dist += Math.abs(c[i] - b);
                    cnt[dist % 2]++;
                }
                if (cnt[0] == 0 || cnt[1] == 0) {
                    answer = "Y";
                }
                // System.out.println(cnt[0]);
                // System.out.println(cnt[1]);
            }
			output(tnum, answer);
		}
	}
}