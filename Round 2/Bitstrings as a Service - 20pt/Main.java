import java.util.Arrays;
import java.util.Scanner;

public class Main
{
    static void output(int testcase, String answer)
    {
        System.out.printf("Case #%d: %s\n", testcase + 1, answer);
    }
	public static void main(String[] args) throws Exception
	{
		Scanner scanner = new Scanner(System.in);
		int T = scanner.nextInt();
		for (int tnum = 0; tnum < T; tnum++)
		{
            // long time0 = System.currentTimeMillis();
			String answer = "";
            int n = scanner.nextInt();
            int m = scanner.nextInt();
            UF uf = new UF(n);
            for (int i = 0; i < m; i++) {
                int x = scanner.nextInt();
                int y = scanner.nextInt();
                for (int j = x - 1, k = y - 1; j < k; j++, k--) {
                    uf.union(j, k);
                }
            }
            int[] flag = new int[n];
            for (int i = 0; i < n; i++) {
                flag[uf.get(i)]++;
            }
            int[] ps = new int[n];
            int pc = 0;
            for (int i = 0; i < n; i++) {
                if (flag[i] > 0) {
                    ps[pc] = flag[i] * n + i;
                    pc++;
                }
            }
            Arrays.sort(ps, 0, pc);
            int ones = 0, zeros = 0;
            for (int i = 0; i < pc; i++) {
                int p = ps[i] % n;
                int size = ps[i] / n;
                if (ones < zeros) {
                    ones += size;
                    flag[p] = 1;
                } else {
                    zeros += size;
                    flag[p] = 0;
                }
            }
            StringBuilder sb = new  StringBuilder(n);
            for (int i = 0; i < n; i++) {
                if (flag[uf.parent[i]] != 0) {
                    sb.append('1');
                } else {
                    sb.append('0');
                }
            }
            answer = sb.toString();
			output(tnum, answer);
            // long time1 = System.currentTimeMillis();
            // System.err.println("time: " + (time1 - time0) + " ms");
		}
	}
}

class UF {
    int[] parent, size;
    UF(int n) {
        parent = new int[n];
        size = new int[n];
        for (int i = 0; i < n; i++) {
            parent[i] = i;
            size[i] = 1;
        }
    }
    int get(int i) {
        int p = parent[i];
        while (p != parent[p]) {
            p = parent[p];
        }
        return parent[i] = p;
    }
    void union(int a, int b) {
        int pa = get(a);
        int pb = get(b);
        if (size[pb] > size[pa]) {
            int t = pa; pa = pb; pb = t;
        }
        size[pa] += size[pb];
        parent[pb] = pa;
    }
}