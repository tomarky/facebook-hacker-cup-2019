import java.util.Scanner;
import javax.script.*;

public class Main
{
    static void output(int testcase, String answer)
    {
        System.out.printf("Case #%d: %s\n", testcase + 1, answer);
    }
	public static void main(String[] args) throws Exception
	{
        ScriptEngineManager mgr = new ScriptEngineManager();
        ScriptEngine eng = mgr.getEngineFactories().get(0).getScriptEngine();
		Scanner scanner = new Scanner(System.in);
		int T = scanner.nextInt();
		for (int tnum = 0; tnum < T; tnum++)
		{
			String answer = "0";
            String E = scanner.next();
            String e01 = E.replace('X', '0').replace('x', '1');
            Object r01 = eng.eval(e01);
            String e10 = E.replace('X', '1').replace('x', '0');
            Object r10 = eng.eval(e10);
            if (!String.valueOf(r01).equals(String.valueOf(r10))) {
                answer = "1";
            }
			output(tnum, answer);
		}
	}
}