import java.util.Scanner;

public class Main
{
    static void output(int testcase, String answer)
    {
        System.out.printf("Case #%d: %s\n", testcase + 1, answer);
    }
	public static void main(String[] args) throws Exception
	{
		Scanner scanner = new Scanner(System.in);
		int T = scanner.nextInt();
		for (int tnum = 0; tnum < T; tnum++)
		{
			String answer = "N";
            String L = scanner.next();
            int dot = 0;
            int bf = 0;
            for (int i = 0; i < L.length(); i++) {
                switch (L.charAt(i)) {
                case '.':
                    dot++;
                    break;
                case 'B':
                    bf++;
                    break;
                }
            }
            if (dot > 0 && bf > 0 && dot <= bf) {
                answer = "Y";
            }
            if (dot > 0 && bf > 1) {
                answer = "Y";
            }
			output(tnum, answer);
		}
	}
}